FROM scratch

ADD bin/vault-watch-docker /vault-watch

CMD ["/vault-watch"]
