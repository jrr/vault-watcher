package controller

import (
	"time"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/apimachinery/pkg/util/wait"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/util/workqueue"
)

const (
	keyPrefix string = "vault-watcher.jrr.io/"
)

// Controller golint
type Controller struct {
	queue    workqueue.RateLimitingInterface
	indexer  cache.Indexer
	informer cache.Controller
}

// New golint
func New(clientset *kubernetes.Clientset) *Controller {
	optsFunc := func(options *metav1.ListOptions) {
		options.LabelSelector = keyPrefix + "kind=watcher"
	}

	watcher := cache.NewFilteredListWatchFromClient(clientset.CoreV1().RESTClient(), "configmaps", corev1.NamespaceAll, optsFunc)

	queue := workqueue.NewRateLimitingQueue(workqueue.DefaultControllerRateLimiter())

	indexer, informer := cache.NewIndexerInformer(watcher, &corev1.ConfigMap{}, 0, cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(obj)
			if err == nil {
				queue.Add(key)
			} else {
				log.Error(err)
			}
		},
		UpdateFunc: func(old interface{}, new interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(new)
			if err == nil {
				queue.Add(key)
			} else {
				log.Error(err)
			}
		},
		DeleteFunc: func(obj interface{}) {
			key, err := cache.DeletionHandlingMetaNamespaceKeyFunc(obj)
			if err == nil {
				queue.Add(key)
			} else {
				log.Error(err)
			}
		},
	}, cache.Indexers{})
	return &Controller{
		queue:    queue,
		indexer:  indexer,
		informer: informer,
	}
}

// Run golint
func (c *Controller) Run(stopCh chan struct{}) {
	defer runtime.HandleCrash()
	defer c.queue.ShutDown()

	log.Info("Starting watcher")

	go c.informer.Run(stopCh)

	if !cache.WaitForCacheSync(stopCh, c.informer.HasSynced) {
		runtime.HandleError(errors.New("timeout waiting for initial cache sync"))
		return
	}

	go wait.Until(c.worker, time.Second, stopCh)

	<-stopCh
	log.Info("Stopping watcher")
}

func (c *Controller) worker() {
	for c.processNextItem() {
	}
}

func (c *Controller) processNextItem() bool {
	key, quit := c.queue.Get()

	if quit {
		return false
	}

	defer c.queue.Done(key)

	keyStr := key.(string)

	log.WithFields(log.Fields{
		"key": keyStr,
	}).Info("Got a key")

	obj, exists, err := c.indexer.GetByKey(keyStr)

	if err != nil {
		log.Error(err)
		return true
	}

	if !exists {
		log.Infof("Watcher %s deleted", keyStr)
	} else {
		log.Infof("Sync watcher %s", obj.(*corev1.ConfigMap).GetName())
	}

	return true
}
