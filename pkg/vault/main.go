package vault

import (
	"io/ioutil"

	"github.com/pkg/errors"

	vault "github.com/hashicorp/vault/api"

	"gitlab.com/jrr/vault-watcher/pkg/config"
)

const (
	tokenFile string = "/var/run/secrets/kubernetes.io/serviceaccount/token"
)

func configure(configObj *config.Config) (*vault.Client, error) {
	vaultConfig := vault.DefaultConfig()

	if configObj.Vault.CACert != "" {
		tlsConfig := &vault.TLSConfig{
			CACert: configObj.Vault.CACert,
		}

		err := vaultConfig.ConfigureTLS(tlsConfig)

		if err != nil {
			return nil, errors.Wrap(err, "failed to set Vault CA cert path")
		}
	}

	client, err := vault.NewClient(vaultConfig)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create Vault client")
	}

	err = client.SetAddress(configObj.Vault.URL)

	if err != nil {
		return nil, errors.Wrap(err, "failed to set Vault address")
	}

	return client, nil
}

// Connect golint
func Connect(configObj *config.Config) (*vault.Logical, error) {
	client, err := configure(configObj)

	if err != nil {
		return nil, errors.Wrap(err, "failed to configure Vault client")
	}

	jwt, err := ioutil.ReadFile(tokenFile)

	if err != nil {
		return nil, errors.Wrapf(err, "failed to read k8s serviceaccount token at %s", tokenFile)
	}

	authBody := map[string]string{
		"role": configObj.Vault.Auth.Role,
		"jwt":  string(jwt),
	}

	authPath := "/v1/auth/" + configObj.Vault.Auth.Path + "/login"

	req := client.NewRequest("POST", authPath)

	if err = req.SetJSONBody(authBody); err != nil {
		return nil, errors.Wrap(err, "failed to set auth JSON body")
	}

	resp, err := client.RawRequest(req)

	if err = req.SetJSONBody(authBody); err != nil {
		return nil, errors.Wrap(err, "failed to authenticate to Vault")
	}

	defer resp.Body.Close()

	rawSecret, err := vault.ParseSecret(resp.Body)

	if err != nil {
		return nil, errors.Wrap(err, "failed to parse Vault response")
	}

	client.SetToken(rawSecret.Auth.ClientToken)

	return client.Logical(), nil
}
