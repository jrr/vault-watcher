package util

import (
	"crypto/sha256"
	"fmt"
)

// StringHash wraps sha256.Sum256 with string conversion
func StringHash(data string) string {
	shaSum := sha256.Sum256([]byte(data))
	return fmt.Sprintf("%x", shaSum)
}
