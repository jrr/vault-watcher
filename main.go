package main

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/jrr/vault-watcher/pkg/config"
	"gitlab.com/jrr/vault-watcher/pkg/controller"
	"gitlab.com/jrr/vault-watcher/pkg/kube"
	"gitlab.com/jrr/vault-watcher/pkg/vault"
)

func main() {
	configure()

	run()
}

func run() {
	clientset, err := kube.Connect()

	if err != nil {
		log.Fatal(err)
	}

	ctrl := controller.New(clientset)

	stopCh := make(chan struct{})
	defer close(stopCh)
	go ctrl.Run(stopCh)

	select {}
}

func configure() {
	configObj, err := config.NewFromFile("/etc/vault-watch/config.yaml")

	if err != nil {
		log.WithFields(log.Fields{
			"stage": "configure",
		}).Fatal(err)
	}

	_, err = vault.Connect(configObj)

	if err != nil {
		log.WithFields(log.Fields{
			"stage": "configure",
		}).Fatal(err)
	}
}

/*
select {
case map <-configmaps.WorkQueue:
  processItem(map)
}

processItem(cfg) is
- vaultHash := GetVaultSecretHash(cfg.vaultPath)
- if cfg.hash and vaultHash != cfg.hash
- - triggerHookAndUpdate(cfg)
- else
- - addHash(cfg)
*/
